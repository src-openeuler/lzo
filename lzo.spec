Name:           lzo
Version:        2.10
Release:        6
Summary:        a real-time data compression library
License:        GPL-2.0-or-later
URL:            https://www.oberhumer.com/opensource/lzo/
Source0:        https://www.oberhumer.com/opensource/lzo/download/%{name}-%{version}.tar.gz
BuildRequires:  gcc
BuildRequires:  zlib-devel
Obsoletes:      %{name}-help < %{version}-%{release}

%description
LZO is a data compression library which is suitable for data
de-/compression in real-time. This means it favours speed
over compression ratio.

%package minilzo
Summary:        miniLZO is a very lightweight subset of the LZO library.
Group:          System Environment/Libraries

%description minilzo
miniLZO is a very lightweight subset of the LZO library intended for
easy inclusion with your application. It is generated automatically
from the LZO source code and contains the most important LZO functions.


%package devel
Summary:        Development files for the lzo library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-minilzo = %{version}-%{release}
Requires:       zlib-devel

%description devel
LZO is a data compression library which is suitable for data
de-/compression in real-time. This means it favours speed
over compression ratio.

%package_help

%prep
%setup -q

%build

%configure --disable-dependency-tracking --disable-static --enable-shared
%make_build

    %{__cc}  %{optflags} -fpic -Iinclude/lzo -o minilzo/minilzo.o -c minilzo/minilzo.c
    %{__cc}  -g -shared -Wl,-z,now -o libminilzo.so.0 -Wl,-soname,libminilzo.so.0 minilzo/minilzo.o


%install
%make_install
%delete_la

rm -fr %{buildroot}%{_datadir}/doc

install -m 755 libminilzo.so.0 $RPM_BUILD_ROOT%{_libdir}
install -p -m 644 minilzo/minilzo.h $RPM_BUILD_ROOT%{_includedir}/lzo
ln -s libminilzo.so.0 $RPM_BUILD_ROOT%{_libdir}/libminilzo.so

%check
%make_build check test

%files
%license COPYING
%doc AUTHORS THANKS NEWS
%{_libdir}/liblzo2.so.*

%files minilzo
%license COPYING
%doc minilzo/README.LZO
%{_libdir}/libminilzo.so.*

%files devel
%doc doc/LZOAPI.TXT doc/LZO.FAQ doc/LZO.TXT
%{_includedir}/lzo
%{_libdir}/libminilzo.so
%{_libdir}/liblzo2.so
%{_libdir}/pkgconfig/lzo2.pc

%changelog
* Fri Mar 14 2025 Funda Wang <fundawang@yeah.net> - 2.10-6
- cleanup spec
- drop help package, it is conflicting with main package

* Mon Mar 18 2024 wangqiang <wangqiang1@kylinos.cn> - 2.10-5
- Optimize build command

* Thu Feb 29 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 2.10-4
- add support for clang 

* Tue Feb 14 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.10-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:optimize test command

* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 2.10-2
- rebuild for next release

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.10-1
- Package init
